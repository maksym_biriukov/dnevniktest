# Dnevnik test
This is a private realization of test task from company Дневник.ру.

[![Swift Version][swift-image]][swift-url]
[![Platform](https://img.shields.io/cocoapods/p/LFAlertController.svg?style=flat)](http://cocoapods.org/pods/LFAlertController)

![](first.png)
![](second.png)
![](third.png)
![](fourth.png)
![](fifth.png)

## Requirements

- iOS 12.0+
- Xcode 11.2

## Usage example

Just run the application in any simulator or device compatible with requirements wrote above.

[swift-image]:https://img.shields.io/badge/swift-5.0-orange.svg
[swift-url]: https://swift.org/
