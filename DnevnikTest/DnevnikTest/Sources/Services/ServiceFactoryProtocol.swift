protocol ServiceFactoryProtocol {
    func makeNetworkService() -> NetworkServiceProtocol
}
