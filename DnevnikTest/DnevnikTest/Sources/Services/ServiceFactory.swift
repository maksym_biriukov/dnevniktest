class ServiceFactory: ServiceFactoryProtocol {
    
    func makeNetworkService() -> NetworkServiceProtocol {
        return NetworkService()
    }
}
