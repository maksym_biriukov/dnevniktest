import Foundation

class NetworkService: NetworkServiceProtocol {
    
    func getItems<T: Decodable>(for term: String, completion: ((Result<T, Error>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "itunes.apple.com"
        urlComponents.path = "/search"
        
        let termItem = URLQueryItem(name: "term", value: term)
        
        // TODO: - Should be injected
        let regionCode = Locale.current.regionCode
        let languageCode = NSLocale.current.identifier
        let countryItem = URLQueryItem(name: "country", value: regionCode)
        let languageItem = URLQueryItem(name: "language", value: languageCode)
        let mediaItem = URLQueryItem(name: "media", value: "movie")

        urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        urlComponents.queryItems = [termItem, mediaItem, countryItem, languageItem]
        
        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            if let error = responseError {
                completion?(.failure(error))
            } else if let jsonData = responseData {
                let decoder = JSONDecoder()
                do {
                    let posts = try decoder.decode(T.self, from: jsonData)
                    completion?(.success(posts))
                } catch {
                    completion?(.failure(error))
                }
            } else {
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                completion?(.failure(error))
            }
        }
        task.resume()
    }
}
