protocol NetworkServiceProtocol: AnyObject {
    func getItems<T: Decodable>(for term: String, completion: ((Result<T, Error>) -> Void)?)
}
