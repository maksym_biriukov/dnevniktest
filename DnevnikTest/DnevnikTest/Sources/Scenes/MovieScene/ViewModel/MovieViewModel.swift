import Foundation

class MovieViewModel {
    
    let viewStateViewModel: MovieViewStateViewModel
    private unowned let navigator: SceneNavigatorProtocol!
    
    init(movie: Movie, navigator: SceneNavigatorProtocol) {
        self.navigator = navigator
        self.viewStateViewModel = .init(with: movie)
    }
            
    // MARK: - Inputs
    
    func bind(dismissAction: inout (() -> Void)?, showPreviewTrailerAction: inout (() -> Void)?) {
        dismissAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.navigator.returnToPreviousScene(animated: true)
        }
        
        showPreviewTrailerAction = { [weak self] in
            guard let self = self, let url = self.viewStateViewModel.previewURL else {
                return
            }
            self.navigator.navigateTo(.preview(url), transitionType: .modal(style: .default, animated: true))
        }
    }
}

