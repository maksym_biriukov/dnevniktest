import Foundation

struct MovieViewStateViewModel {
    
    let movieName: String
    let description: String?
    let genre: String
    let totalPrice: String?
    let rentPrice: String?
    let contentAdvisoryRating: String
    
    let smallAssetURL: URL?
    let largeAssetURL: URL?
    let previewURL: URL?
    
    init(with movie: Movie) {
        self.movieName = movie.trackName
        self.smallAssetURL = movie.artWorkUrl300
        self.largeAssetURL = movie.artWorkUrl500
        self.previewURL = movie.previewUrl
        self.description = movie.longDescription
        self.genre = String(format: "movie_genre".localized, movie.primaryGenreName)
        if let totalPrice = movie.trackPrice {
            self.totalPrice = String(format: "movie_total_price".localized, "\(totalPrice)", movie.currency)
        } else {
            self.totalPrice = nil
        }
        if let rentPrice = movie.trackRentalPrice {
            self.rentPrice = String(format: "movie_rent_price".localized, "\(rentPrice)", movie.currency)
        } else {
            self.rentPrice = nil
        }
        self.contentAdvisoryRating = String(format: "content_advisory_rating".localized, movie.contentAdvisoryRating)
    }
}
