import UIKit
import AVKit

extension SceneFactory {
    
    static func makeMovieScene(movie: Movie, navigator: SceneNavigatorProtocol) -> UIViewController {
        
        let viewModel = MovieViewModel(movie: movie, navigator: navigator)
        let movieScene = MovieScene(viewModel: viewModel)
        
        return movieScene
    }
    
    static func makePlayerScene(url: URL) -> UIViewController {
        
        let player = AVPlayer(url: url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        return playerController
    }
}
