import UIKit

class MovieScene: UIViewController {
    
    // MARK: - Outlets & Variables
    
    private var movieView: MovieView {
        return view as! MovieView
    }

    private let viewModel: MovieViewModel

    // MARK: - Initialization
    
    required init(viewModel: MovieViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = MovieView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.bind(dismissAction: &movieView.closeTapped, showPreviewTrailerAction: &movieView.playTapped)
        
        movieView.viewStateViewModel = viewModel.viewStateViewModel
    }
}
