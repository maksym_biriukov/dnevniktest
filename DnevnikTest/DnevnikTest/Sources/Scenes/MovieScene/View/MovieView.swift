import UIKit

class MovieView: UIView {
    
    var closeTapped: (() -> Void)?
    var playTapped: (() -> Void)?
    
    var viewStateViewModel: MovieViewStateViewModel? {
        didSet {
            nameLabel.text = viewStateViewModel?.movieName
            descriptionLabel.text = viewStateViewModel?.description
            genreLabel.text = viewStateViewModel?.genre
            priceTotalLabel.text = viewStateViewModel?.totalPrice
            priceRentLabel.text = viewStateViewModel?.rentPrice
            contentAdvisoryRatingLabel.text = viewStateViewModel?.contentAdvisoryRating
            
            if viewStateViewModel?.totalPrice == nil {
                priceTotalLabel.isHidden = true
            }
            
            if viewStateViewModel?.rentPrice == nil {
                priceRentLabel.isHidden = true
            }
            
            if viewStateViewModel?.previewURL == nil {
                playButton.isEnabled = false
            }
            
            if let url = viewStateViewModel?.smallAssetURL {
                posterBlurredImageView.downloadImage(from: url, contentMode: .scaleAspectFill, shouldBlur: true)
            }
            if let url = viewStateViewModel?.largeAssetURL {
                posterImageView.downloadImage(from: url, contentMode: .scaleAspectFit)
            }
        }
    }
    
    // MARK: - Outlets
    
    private let closeButton: UIButton = {
        let close = UIButton()
        
        close.setImage(UIImage(named: "close"), for: .normal)
        close.contentEdgeInsets = UIEdgeInsets(top: 12.0, left: 12.0, bottom: 12.0, right: 12.0)
        close.backgroundColor = UIColor.applicationFillColor.withAlphaComponent(0.8)
        close.tintColor = .applicationTintColor
        close.translatesAutoresizingMaskIntoConstraints = false
        close.addTarget(self, action: #selector(shouldDismiss), for: .touchUpInside)
        return close
    }()
    
    private let playButton: UIButton = {
        let play = UIButton()
        
        play.setImage(UIImage(named: "play"), for: .normal)
        play.contentEdgeInsets = UIEdgeInsets(top: 12.0, left: 12.0, bottom: 12.0, right: 12.0)
        play.backgroundColor = UIColor.applicationFillColor.withAlphaComponent(0.8)
        play.tintColor = .applicationTintColor
        play.translatesAutoresizingMaskIntoConstraints = false
        play.addTarget(self, action: #selector(shouldPlayTrailer), for: .touchUpInside)
        return play
    }()
    
    private let posterBlurredImageView: UIImageView = {
        let posterImage = UIImageView()
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        posterImage.backgroundColor = .applicationContentColor
        posterImage.tintColor = .applicationFillColor
        posterImage.contentMode = .scaleAspectFit
        posterImage.layer.masksToBounds = true
        return posterImage
    }()
    
    private let posterContainerView: UIView = {
        let posterContainer = UIView()
        posterContainer.translatesAutoresizingMaskIntoConstraints = false
        posterContainer.clipsToBounds = false
        return posterContainer
    }()
    
    private let posterImageView: UIImageView = {
        let posterImage = UIImageView()
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        posterImage.backgroundColor = .applicationContentColor
        posterImage.tintColor = .applicationFillColor
        posterImage.contentMode = .center
        posterImage.clipsToBounds = true
        posterImage.isUserInteractionEnabled = true
        return posterImage
    }()
    
    private let nameLabel: UILabel = {
        let name: UILabel = UILabel()
        name.lineBreakMode = .byWordWrapping
        name.numberOfLines = 2
        name.textAlignment = .center
        name.adjustsFontForContentSizeCategory = true
        name.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        return name
    }()
    
    private let descriptionLabel: UILabel = {
        let description: UILabel = UILabel()
        description.lineBreakMode = .byWordWrapping
        description.numberOfLines = 0
        description.textAlignment = .left
        description.adjustsFontForContentSizeCategory = true
        description.font = UIFont.preferredFont(forTextStyle: .body)
        return description
    }()
    
    private let priceTotalLabel: UILabel = {
        let priceTotal: UILabel = UILabel()
        priceTotal.lineBreakMode = .byWordWrapping
        priceTotal.textAlignment = .left
        priceTotal.adjustsFontForContentSizeCategory = true
        priceTotal.font = UIFont.preferredFont(forTextStyle: .title3)
        return priceTotal
    }()
    
    private let priceRentLabel: UILabel = {
        let priceRent: UILabel = UILabel()
        priceRent.lineBreakMode = .byWordWrapping
        priceRent.textAlignment = .left
        priceRent.adjustsFontForContentSizeCategory = true
        priceRent.font = UIFont.preferredFont(forTextStyle: .title3)
        return priceRent
    }()
    
    private let genreLabel: UILabel = {
        let genre: UILabel = UILabel()
        genre.lineBreakMode = .byWordWrapping
        genre.textAlignment = .left
        genre.adjustsFontForContentSizeCategory = true
        genre.font = UIFont.preferredFont(forTextStyle: .title3)
        return genre
    }()
    
    private let contentAdvisoryRatingLabel: UILabel = {
        let advisoryRating: UILabel = UILabel()
        advisoryRating.lineBreakMode = .byWordWrapping
        advisoryRating.textAlignment = .left
        advisoryRating.adjustsFontForContentSizeCategory = true
        advisoryRating.font = UIFont.preferredFont(forTextStyle: .title3)
        return advisoryRating
    }()
    
    private let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.isScrollEnabled = true
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    private let generalStackView: UIStackView = {
        let generalStack = UIStackView()
        generalStack.translatesAutoresizingMaskIntoConstraints = false
        generalStack.axis = .vertical
        generalStack.spacing = 8.0
        return generalStack
    }()
    
    private let generalContainerView: UIView = {
        let generalContainer = UIView()
        if #available(iOS 13.0, *) {
            generalContainer.backgroundColor = UIColor.systemBackground
        } else {
            generalContainer.backgroundColor = UIColor.white
        }
        generalContainer.translatesAutoresizingMaskIntoConstraints = false
        return generalContainer
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    // MARK: - Private
    
    private func setupViews() {
        if #available(iOS 13.0, *) {
            backgroundColor = .systemBackground
        } else {
            backgroundColor = .white
        }
        
        addSubview(posterBlurredImageView)
        addSubview(scrollView)
        addSubview(closeButton)
        
        scrollView.addSubview(generalContainerView)
        scrollView.addSubview(posterContainerView)
        
        generalContainerView.addSubview(generalStackView)
        
        generalStackView.addArrangedSubview(nameLabel)
        generalStackView.addArrangedSubview(genreLabel)
        generalStackView.addArrangedSubview(contentAdvisoryRatingLabel)
        generalStackView.addArrangedSubview(priceTotalLabel)
        generalStackView.addArrangedSubview(priceRentLabel)
        generalStackView.addArrangedSubview(descriptionLabel)
        
        posterContainerView.addSubview(posterImageView)
        posterImageView.addSubview(playButton)
        
        // scroll
        scrollView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        // container
        generalContainerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        generalStackView.topAnchor.constraint(equalTo: generalContainerView.topAnchor, constant: 20.0).isActive = true
        generalStackView.bottomAnchor.constraint(equalTo: generalContainerView.bottomAnchor, constant: -8.0).isActive = true
        generalStackView.leadingAnchor.constraint(equalTo: generalContainerView.leadingAnchor, constant: 8.0).isActive = true
        generalStackView.trailingAnchor.constraint(equalTo: generalContainerView.trailingAnchor, constant: -8.0).isActive = true
        
        // buttons
        closeButton.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        closeButton.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16.0).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 34.0).isActive = true
        closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor).isActive = true
        
        playButton.centerXAnchor.constraint(equalTo: posterImageView.centerXAnchor).isActive = true
        playButton.centerYAnchor.constraint(equalTo: posterImageView.centerYAnchor).isActive = true
        playButton.widthAnchor.constraint(equalToConstant: 34.0).isActive = true
        playButton.heightAnchor.constraint(equalTo: playButton.widthAnchor).isActive = true
        
        // images
        posterContainerView.leadingAnchor.constraint(equalTo: posterImageView.leadingAnchor).isActive = true
        posterContainerView.trailingAnchor.constraint(equalTo: posterImageView.trailingAnchor).isActive = true
        posterContainerView.topAnchor.constraint(equalTo: posterImageView.topAnchor).isActive = true
        posterContainerView.bottomAnchor.constraint(equalTo: posterImageView.bottomAnchor).isActive = true
        
        posterImageView.centerXAnchor.constraint(equalTo: posterBlurredImageView.centerXAnchor).isActive = true
        posterImageView.bottomAnchor.constraint(equalTo: generalContainerView.topAnchor, constant: 20.0).isActive = true
        posterImageView.widthAnchor.constraint(equalTo: posterBlurredImageView.widthAnchor, multiplier: 1/3).isActive = true
        posterImageView.heightAnchor.constraint(equalTo: posterImageView.widthAnchor, multiplier: 3/2).isActive = true
        
        posterBlurredImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        posterBlurredImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        posterBlurredImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        posterBlurredImageView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        posterBlurredImageView.heightAnchor.constraint(equalTo: widthAnchor).isActive = true
    }
    
    @objc
    private func shouldDismiss() {
        closeTapped?()
    }
    
    @objc
    private func shouldPlayTrailer() {
        playTapped?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        closeButton.layer.cornerRadius = closeButton.frame.width / 2
        playButton.layer.cornerRadius = playButton.frame.width / 2
        
        posterContainerView.layer.shadowColor = UIColor.black.cgColor
        posterContainerView.layer.shadowOpacity = 0.4
        posterContainerView.layer.shadowOffset = .zero
        posterContainerView.layer.shadowRadius = 4
        posterContainerView.layer.shadowPath = UIBezierPath(roundedRect: posterContainerView.bounds, cornerRadius: 10).cgPath
        
        posterImageView.layer.cornerRadius = 10.0
        
        let path = UIBezierPath(roundedRect: generalContainerView.bounds,
                                byRoundingCorners: [.topLeft, .topRight],
                                cornerRadii: CGSize(width: 16.0, height: 16.0))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        generalContainerView.layer.mask = maskLayer
        
        let difference = posterBlurredImageView.frame.height - posterImageView.frame.height
        scrollView.contentInset = UIEdgeInsets(top: difference, left: 0.0, bottom: difference, right: 0.0)
        scrollView.contentSize = generalContainerView.bounds.size
    }
}
