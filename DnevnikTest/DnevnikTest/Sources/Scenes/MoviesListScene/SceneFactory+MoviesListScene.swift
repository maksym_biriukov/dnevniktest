import UIKit

extension SceneFactory {
    
    static func makeMoviesListScene(navigator: SceneNavigatorProtocol, networkService: NetworkServiceProtocol) -> UIViewController {
        
        let viewModel = MoviesViewModel(navigator: navigator, networkService: networkService)
        let mainScene = MoviesListScene(viewModel: viewModel)
        let navigationController = UINavigationController(rootViewController: mainScene)
        
        navigationController.navigationBar.prefersLargeTitles = true
        
        mainScene.title = "movies_list_navigation_title".localized
        
        return navigationController
    }
}
