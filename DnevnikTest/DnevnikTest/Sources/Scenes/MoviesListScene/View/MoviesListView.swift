import UIKit

protocol MoviesListDatasourceDelegate: AnyObject {
    func numberOfMovies() -> Int
    func movieCellViewModel(at indexPath: IndexPath) -> MovieCellViewModel
    func movieSelected(at indexPath: IndexPath)
    func shouldSearch(term: String)
}

class MoviesListView: UIView {
        
    weak var delegateDatasource: MoviesListDatasourceDelegate? = nil
    
    // MARK: - Outlets
    
    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        
        if #available(iOS 13.0, *) {
            backgroundColor = .systemBackground
            collectionView.backgroundColor = .systemBackground
        } else {
            backgroundColor = .white
            collectionView.backgroundColor = .white
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(cellType: SearchMoviewCollectionViewCell.self)
        collectionView.register(cellType: MovieCollectionViewCell.self)
        
        collectionView.allowsSelection = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    private let collectionLayout: RSKCollectionViewRetractableFirstItemLayout = {
        let layout = RSKCollectionViewRetractableFirstItemLayout()
        layout.firstItemRetractableAreaInset = UIEdgeInsets(top: 8.0, left: 0.0, bottom: 8.0, right: 0.0)
        layout.scrollDirection = .vertical
        return layout
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activity: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            activity = UIActivityIndicatorView(style: .medium)
        } else {
            activity = UIActivityIndicatorView(style: .gray)
        }
        activity.isHidden = true
        activity.translatesAutoresizingMaskIntoConstraints = false
        return activity
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func showActivityIndicator() {
        bringSubviewToFront(activityIndicator)
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        collectionView.isUserInteractionEnabled = false
    }
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        collectionView.isUserInteractionEnabled = true
    }

    // MARK: - Private
    
    private func setupViews() {
        addSubview(collectionView)
        addSubview(activityIndicator)

        activityIndicator.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true

        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension MoviesListView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return delegateDatasource?.numberOfMovies() ?? 0
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
            case 0:
                let cell = collectionView.dequeueReusableCell(for: indexPath) as SearchMoviewCollectionViewCell
                cell.searchBar.delegate = self
                return cell
            case 1:
                let cell = collectionView.dequeueReusableCell(for: indexPath) as MovieCollectionViewCell
                cell.cellViewModel = delegateDatasource?.movieCellViewModel(at: indexPath)
                return cell
            default:
                fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegateDatasource?.movieSelected(at: indexPath)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MoviesListView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            let itemWidth = collectionView.frame.width
            let itemHeight: CGFloat = 44.0
            return CGSize(width: itemWidth, height: itemHeight)
        case 1:
            let prefferedSize = CGSize(width: 100.0, height: 150)
            let calculated = LayoutCalculatingHelper.automaticThumbnailSize(for: prefferedSize,
                                                                            containerSize: collectionView.bounds.size,
                                                                            interitemSpacing: 20.0)
            return calculated
        default:
            assert(false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets(top: 10.0, left: 2.0, bottom: 0.0, right: 2.0)
        case 1:
            return UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        default:
            assert(false)
        }
    }
    
    // MARK: - UIScrollViewDelegate
    
    internal func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let indexPath = IndexPath(item: 0, section: 0)

        guard scrollView === self.collectionView,
            let cell = self.collectionView.cellForItem(at: indexPath) as? SearchMoviewCollectionViewCell,
            cell.searchBar.isFirstResponder else {
            return
        }
        
        cell.searchBar.resignFirstResponder()
    }
}

// MARK: - UISearchBarDelegate

extension MoviesListView: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let term = searchBar.text, !term.isEmpty else {
            return
        }
        
        searchBar.resignFirstResponder()
        
        delegateDatasource?.shouldSearch(term: term)
    }
}
