import UIKit

class MoviesListScene: UIViewController {
    
    // MARK: - Outlets & Variables
    
    private var moviesView: MoviesListView {
        return view as! MoviesListView
    }
    
    private var viewModel: MoviesViewModel
    
    // MARK: - Initialization
    
    required init(viewModel: MoviesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = MoviesListView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = []
        
        bindUI()
    }
    
    private func bindUI() {
        moviesView.delegateDatasource = self
        
        viewModel.reloadData = { [weak self] in
            DispatchQueue.main.async {
                self?.moviesView.collectionView.reloadData()
            }
        }
        
        viewModel.showError = { [weak self] in
            DispatchQueue.main.async {
                self?.showError()
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                if self.viewModel.isLoading {
                    self.moviesView.showActivityIndicator()
                } else {
                    self.moviesView.hideActivityIndicator()
                }
            }
        }
    }
    
    private func showError() {
        let alert = UIAlertController(title: "error_title".localized, message: viewModel.error?.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok_title".localized, style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension MoviesListScene: MoviesListDatasourceDelegate {
    
    func numberOfMovies() -> Int {
        return viewModel.itemsCount
    }
    
    func movieCellViewModel(at indexPath: IndexPath) -> MovieCellViewModel {
        viewModel.cellViewModel(for: indexPath)
    }
    
    func movieSelected(at indexPath: IndexPath) {
        viewModel.shouldShowMovie(at: indexPath)
    }
    
    func shouldSearch(term: String) {
        viewModel.searchTerm = term
    }
}
