import UIKit

class MovieCollectionViewCell: UICollectionViewCell, Reusable {
    
    // MARK: - Outlets
    
    let containerView: UIView = {
        let container = UIView()
        container.clipsToBounds = true
        container.layer.cornerRadius = 16.0
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .applicationContentColor
        return container
    }()
    
    let assetImageView: UIImageView = {
        let asset = UIImageView()
        asset.contentMode = .center
        asset.image = UIImage(named: "movie")
        asset.translatesAutoresizingMaskIntoConstraints = false
        asset.tintColor = .applicationFillColor
        return asset
    }()
    
    // MARK: - Binding
    
    var cellViewModel: MovieCellViewModel? {
        didSet {
            if let url = cellViewModel?.assetURL {
                assetImageView.downloadImage(from: url, contentMode: .scaleToFill)
            }
        }
    }
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        assetImageView.contentMode = .center
        assetImageView.image = UIImage(named: "movie")
    }
    
    func setupViews() {
        contentView.addSubview(containerView)
        containerView.addSubview(assetImageView)
        
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        assetImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        assetImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        assetImageView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        assetImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
