import UIKit

class SearchMoviewCollectionViewCell: UICollectionViewCell, Reusable {
    
    var searchBar: UISearchBar = {
        let search = UISearchBar()
        search.searchBarStyle = .minimal
        return search
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(searchBar)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        searchBar.frame = CGRect(origin: CGPoint.zero, size: self.contentView.bounds.size)
    }
    
    override class var requiresConstraintBasedLayout: Bool {
        return false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
