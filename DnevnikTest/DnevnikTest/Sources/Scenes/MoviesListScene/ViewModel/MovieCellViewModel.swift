import Foundation

struct MovieCellViewModel {
    
    var movieName: String?
    var assetURL: URL?

    init(with movie: Movie) {
        self.movieName = movie.trackName
        self.assetURL = movie.artWorkUrl300
    }
}
