import Foundation

class MoviesViewModel {
    
    typealias MovieResponseType = ListResponse<Movie>
    
    init(navigator: SceneNavigatorProtocol, networkService: NetworkServiceProtocol) {
        self.navigator = navigator
        self.networkService = networkService
    }
    
    // MARK: - Outputs
    
    var reloadData: (() -> Void)?
    var showError: (() -> Void)?
    var updateLoadingStatus: (() -> Void)?
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var error: Error? = .none {
        didSet {
            self.showError?()
        }
    }
    
    var itemsCount: Int {
        return datasource.count
    }
    
    func cellViewModel(for indexPath: IndexPath) -> MovieCellViewModel {
        return MovieCellViewModel.init(with: datasource[indexPath.row])
    }
    
    // MARK: - Inputs
    
    var searchTerm: String = "" {
        didSet {
            fetchMovies()
        }
    }
    
    func shouldShowMovie(at indexPath: IndexPath) {
        let model = datasource[indexPath.row]
        navigator.navigateTo(.movie(model), transitionType: .modal(style: .fullScreen, animated: true))
    }
    
    // MARK: - Private
    
    private var datasource = [Movie]() {
        didSet {
            self.reloadData?()
        }
    }
    
    private unowned var navigator: SceneNavigatorProtocol!
    private unowned var networkService: NetworkServiceProtocol!
    
    private func fetchMovies() {
        guard !isLoading else {
            return
        }
        
        self.datasource = []
        
        isLoading = true
        
        networkService.getItems(for: searchTerm) { [weak self] (result: Result<MovieResponseType, Error>) in
            guard let self = self else {
                return
            }
            
            self.isLoading = false
            
            switch result {
            case .success(let response):
                self.datasource = response.results
            case .failure(let error):
                self.error = error
            }
        }
    }
}
