import Foundation

struct Movie: Decodable {
    
    let trackId: Int
    
    let trackName: String
    let artistName: String
    let primaryGenreName: String
    let shortDescription: String?
    let longDescription: String?
    let contentAdvisoryRating: String
    let currency: String

    let trackPrice: Double?
    let trackRentalPrice: Double?
    
    let previewUrl: URL?
    private let artworkUrl100: URL?
    
    /// This is unofficial trick used to load more precise images as a background, because of we have no possibility to parse more precise images from itunes public response api.
    var artWorkUrl300: URL? {
        let smallArtwork = artworkUrl100
        guard let replaced = smallArtwork?.absoluteString.replacingOccurrences(of: "100x100", with: "300x300"),
            let url = URL(string: replaced) else {
                return .none
        }
        return url
    }
    
    /// This is unofficial trick used to load more precise images as a background, because of we have no possibility to parse more precise images from itunes public response api.
    var artWorkUrl500: URL? {
        let smallArtwork = artworkUrl100
        guard let replaced = smallArtwork?.absoluteString.replacingOccurrences(of: "100x100", with: "500x500"),
            let url = URL(string: replaced) else {
                return .none
        }
        return url
    }
}
