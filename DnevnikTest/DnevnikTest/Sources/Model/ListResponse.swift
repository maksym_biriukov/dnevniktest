struct ListResponse<T: Decodable>: Decodable {
    var resultCount: Int
    var results: [T]
}
