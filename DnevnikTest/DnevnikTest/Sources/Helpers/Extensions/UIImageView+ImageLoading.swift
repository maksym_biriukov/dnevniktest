import UIKit

extension UIImageView {
    
    func downloadImage(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit, shouldBlur: Bool = false) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                var image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                if shouldBlur {
                    image = image.blurred() ?? image
                }
                self.image = image
                self.contentMode = mode
            }
        }.resume()
    }
    
}
