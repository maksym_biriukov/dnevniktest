import Foundation

enum Scene {
    case movies
    case movie(Movie)
    case preview(URL)
}
