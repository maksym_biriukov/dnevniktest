import UIKit

class CompositionRoot {
    
    unowned var navigator: SceneNavigatorProtocol!
    private let serviceFactory: ServiceFactoryProtocol

    private let networkService: NetworkServiceProtocol

    init(serviceFactory: ServiceFactoryProtocol) {
        self.serviceFactory = serviceFactory
        self.networkService = serviceFactory.makeNetworkService()
    }
    
    func composeScene(_ scene: Scene) -> UIViewController {
        switch scene {
        case .movies:
            return SceneFactory.makeMoviesListScene(navigator: navigator, networkService: networkService)
        case .movie(let movie):
            return SceneFactory.makeMovieScene(movie: movie, navigator: navigator)
        case .preview(let url):
            return SceneFactory.makePlayerScene(url: url)
        }
    }
}
