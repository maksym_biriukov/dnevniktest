import UIKit

protocol SceneNavigatorProtocol: class {
    
    func initViewControllerHierarchy(withRootScene rootScene: Scene)
    
    func navigateTo(_ scene: Scene, transitionType: SceneTransitionType)
    func returnToPreviousScene(animated: Bool, completion: (() -> Void)?)
}

extension SceneNavigatorProtocol {
    
    func returnToPreviousScene(animated: Bool) {
        returnToPreviousScene(animated: animated, completion: nil)
    }
}

protocol SceneNavigatorFlowProtocol: SceneNavigatorProtocol {
    var currentViewController: UIViewController! { get }
}
