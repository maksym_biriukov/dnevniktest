import UIKit

enum ModalTransitionType {
    case `default`
    case fullScreen
    case crossDisolve
    case transition(destination: UIViewController?)
}

enum SceneTransitionType {
    case root                                                   // make view controller the root view controller
    case push                                                   // push view controller to navigation stack
    case modal(style: ModalTransitionType, animated: Bool)      // present view controller modally
}
