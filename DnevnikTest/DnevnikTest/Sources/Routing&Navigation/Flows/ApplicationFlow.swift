import UIKit

class ApplicationFlow: Flow {
    
    private let navigator: SceneNavigatorFlowProtocol
    private let compositionRoot: CompositionRoot
        
    init(compositionRoot: CompositionRoot) {
        let window = UIWindow(frame: UIScreen.main.bounds)
        if #available(iOS 13.0, *) {
            window.backgroundColor = .systemBackground
        } else {
            window.backgroundColor = .white
        }
        
        self.compositionRoot = compositionRoot
        self.navigator = SceneNavigator(window: window, compositionRoot: compositionRoot)
        self.compositionRoot.navigator = navigator
    }
    
    func start(transitionType: SceneTransitionType) {
       navigator.initViewControllerHierarchy(withRootScene: .movies)
    }
}
