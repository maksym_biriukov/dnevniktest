protocol Flow {
    func start(transitionType: SceneTransitionType)
}
