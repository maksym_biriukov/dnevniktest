//
//  AppDelegate.swift
//  DnevnikTest
//
//  Created by Maksym Biriukov on 27/11/2019.
//  Copyright © 2019 dnevnik.test. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private var compositionRoot: CompositionRoot!
    private var applicationFlow: ApplicationFlow!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                
        compositionRoot = makeCompositionRoot()
        applicationFlow = ApplicationFlow(compositionRoot: compositionRoot)
        applicationFlow.start(transitionType: .root)
        
        return true
    }
}

extension AppDelegate {
    
    func makeCompositionRoot() -> CompositionRoot {
        let factory = ServiceFactory()
        return CompositionRoot(serviceFactory: factory)
    }
}
